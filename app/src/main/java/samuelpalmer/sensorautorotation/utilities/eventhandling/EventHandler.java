package samuelpalmer.sensorautorotation.utilities.eventhandling;

/**
 * Does something in response to an event
 */
public interface EventHandler {
    void update();
}
