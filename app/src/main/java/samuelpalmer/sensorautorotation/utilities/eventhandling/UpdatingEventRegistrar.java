package samuelpalmer.sensorautorotation.utilities.eventhandling;

/**
 * An event that can be triggered externally by calling {@link #report()}.
 */
public class UpdatingEventRegistrar extends EventRegistrar {

    @Override
    public void report() {
        super.report();
    }

}
