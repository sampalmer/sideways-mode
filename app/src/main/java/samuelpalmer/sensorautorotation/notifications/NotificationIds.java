package samuelpalmer.sensorautorotation.notifications;

/**
 * Contains the each notification's unique identifier to be used when posting the notification to the OS
 */
public class NotificationIds {
    public static final int SERVICE = 1;
    public static final int UPGRADE = 2;
    public static final int ERROR = 3;
    public static final int PERMISSION = 4;
    public static final int DUMMY = 5;
}
