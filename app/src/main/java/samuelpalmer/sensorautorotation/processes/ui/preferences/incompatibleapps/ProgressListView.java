package samuelpalmer.sensorautorotation.processes.ui.preferences.incompatibleapps;

import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;

class ProgressListView {
    public ArrayAdapter<InstalledApp> adapter;
    public ListView list;
    public ProgressBar progressBar;
}
