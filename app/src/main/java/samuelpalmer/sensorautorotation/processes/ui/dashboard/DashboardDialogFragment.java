package samuelpalmer.sensorautorotation.processes.ui.dashboard;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import samuelpalmer.sensorautorotation.R;
import samuelpalmer.sensorautorotation.processes.ui.preferences.SettingsActivity;
import samuelpalmer.sensorautorotation.utilities.ViewUtilities;
import samuelpalmer.sensorautorotation.utilities.eventhandling.EventHandler;

/**
 * The dialog for the app's main dashboard
 */
public class DashboardDialogFragment extends AppCompatDialogFragment {

    private static final String TAG_NORMAL_CONTENTS_FRAGMENT = "normalContents";
    private static final String TAG_PERMISSION_CONTENTS_FRAGMENT = "permissionContents";
    private static final String TAG_QUICK_SETTING_CONTENTS_FRAGMENT = "quickSettingContents";
    private static final String TAG_TIPS_FRAGMENT = "tips";

    private DashboardPermissionFragment permissionSection;
    private DashboardQuickSettingFragment quickSettingSection;
    private DashboardNormalFragment normalSection;
    private FirstStartTipsFragment tipsFragment;
    private ImageButton menuButton;
    private PopupMenu popup;

    public DashboardDialogFragment() {
        setStyle(STYLE_NO_TITLE, 0);
    }

    @Override
    public int getTheme() {
        TypedValue outValue = new TypedValue();
        Context context = getContext();
        assert context != null;
        context.getTheme().resolveAttribute(androidx.appcompat.R.attr.alertDialogTheme, outValue, true);
        return outValue.resourceId;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_dashboard, container, false);

        menuButton = view.findViewById(R.id.dashboard_menu);
        assert menuButton != null;
        menuButton.setOnClickListener(v -> popup.show());

        popup = new PopupMenu(requireActivity(), menuButton);
        popup.setOnMenuItemClickListener(this::handleMenuItemClick);
        popup.inflate(R.menu.main);
        menuButton.setOnTouchListener(popup.getDragToOpenListener());

        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        permissionSection = (DashboardPermissionFragment) fragmentManager.findFragmentByTag(TAG_PERMISSION_CONTENTS_FRAGMENT);
        if (permissionSection == null) {
            permissionSection = new DashboardPermissionFragment();
            fragmentTransaction.add(R.id.dashboard_permission_contents, permissionSection, TAG_PERMISSION_CONTENTS_FRAGMENT);
        }

        quickSettingSection = (DashboardQuickSettingFragment) fragmentManager.findFragmentByTag(TAG_QUICK_SETTING_CONTENTS_FRAGMENT);
        if (quickSettingSection == null) {
            quickSettingSection = new DashboardQuickSettingFragment();
            fragmentTransaction.add(R.id.dashboard_quick_setting_contents, quickSettingSection, TAG_QUICK_SETTING_CONTENTS_FRAGMENT);
        }

        normalSection = (DashboardNormalFragment) fragmentManager.findFragmentByTag(TAG_NORMAL_CONTENTS_FRAGMENT);
        if (normalSection == null) {
            normalSection = new DashboardNormalFragment();
            fragmentTransaction.add(R.id.dashboard_normal_contents, normalSection, TAG_NORMAL_CONTENTS_FRAGMENT);
        }

        tipsFragment = (FirstStartTipsFragment) fragmentManager.findFragmentByTag(TAG_TIPS_FRAGMENT);
        if (tipsFragment == null) {
            tipsFragment = new FirstStartTipsFragment();
            fragmentTransaction.add(tipsFragment, TAG_TIPS_FRAGMENT);
        }

        fragmentTransaction.commit();

        tipsFragment.setStatus(normalSection.getStatus());
        normalSection.setStatusChangeListener(status -> tipsFragment.setStatus(status));

        return view;
    }

    public void setCommandSentListener(DashboardNormalFragment.CommandSentListener commandSentListener) {
        normalSection.setCommandSentListener(commandSentListener);
    }

    @Override
    public void onCancel(@NonNull DialogInterface dialog) {
        super.onCancel(dialog);

        // It's important that we finish the activity when the dialog is *cancelled*, not dismissed.
        // This is because dismissal also occurs when the screen orientation or layout changes.
        // Hence if we finished the activity on *dismiss*, the dialog would disappear each time the screen orientation changes.
        requireActivity().finish();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        DashboardDialogViewModel model = new ViewModelProvider(requireActivity()).get(DashboardDialogViewModel.class);
        model.getDashboardSection().observe(this, section -> {
            ViewUtilities.ensureVisibility(permissionSection.requireView(), section == DashboardSection.PERMISSION ? View.VISIBLE : View.GONE);
            ViewUtilities.ensureVisibility(quickSettingSection.requireView(), section == DashboardSection.QUICK_SETTING ? View.VISIBLE : View.GONE);
            ViewUtilities.ensureVisibility(normalSection.requireView(), section == DashboardSection.NORMAL ? View.VISIBLE : View.GONE);
            tipsFragment.setNormalSectionVisibility(section == DashboardSection.NORMAL);
        });
    }

    @Override
    public void onStart() {
        super.onStart();

        if (!tipsFragment.targetsSet())
            tipsFragment.setTargets(getDialog(), normalSection.getRightSideButton(), menuButton, normalSection.getOffButton());

        tipsFragment.onCanShowTipsChanged.subscribe(updateShowTipsState);
        updateShowTipsState.update();
    }

    @Override
    public void onStop() {
        super.onStop();

        tipsFragment.onCanShowTipsChanged.unsubscribe(updateShowTipsState);
    }

    private final EventHandler updateShowTipsState = new EventHandler() {
        @Override
        public void update() {
            Menu menu = popup.getMenu();
            MenuItem tipsMenuItem = menu.findItem(R.id.action_tips);
            boolean canShowTips = tipsFragment.canShowTips();
            tipsMenuItem.setEnabled(canShowTips);
        }
    };

    @SuppressWarnings("SameReturnValue")
    private boolean handleMenuItemClick(MenuItem menuItem) {
        /*
            The dashboard activity needs to run in its own task that does not show up in the recents screen so it feels like a dialog rather than a full activity.
            However, the activities we launch from the dashboard should have normal activity behaviour, so we launch them into a separate task.
         */
        int flags = Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK;

        int itemId = menuItem.getItemId();
        if (itemId == R.id.action_tips)
            tipsFragment.showAllTips();
        else if (itemId == R.id.action_settings) {
            Intent settingsIntent = new Intent(requireContext(), SettingsActivity.class).addFlags(flags);

            DashboardActivity dashboardActivity = (DashboardActivity) requireActivity();
            dashboardActivity.unlockAndRun(() -> startActivity(settingsIntent));
        } else
            throw new RuntimeException("Unknown navigation menu item: " + itemId);

        return true;
    }

}
