package samuelpalmer.sensorautorotation.processes.ui.dashboard;

/**
 * The overall mode of the dashboard UI
 */
enum DashboardSection {
    PERMISSION,
    QUICK_SETTING,
    NORMAL,
}
