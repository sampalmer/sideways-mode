package samuelpalmer.sensorautorotation.components;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Use this activity within automated tests to provide a reliable activity screen orientation
 * preference state, particularly to allow allow screen orientations
 */
public class TestActivity extends AppCompatActivity {
}
