### The app says my device doesn't have a working sensor/accelerometer
### My rotation sensor / gravity sensor / accelerometer is broken, but this app won't rotate the screen
### The app won't let me start it without a working sensor, but I want to use it anyway

Auto-rotate uses your phone's accelerometer sensor to detect which way you're holding the phone. Unfortunately, your sensor isn't working, so you cannot use auto-rotate.

Sideways Mode is an enhancement to auto-rotate, so you can't use Sideways Mode either. If you are trying to use Sideways Mode for something other than auto-rotate, then you will need to look elsewhere.

If your phone's built-in auto-rotate feature is working but Sideways Mode still says the sensor isn't working, then there might be an issue in Sideways Mode.

### The app doesn't rotate my launcher / lock screen / {some app}

Check whether the app rotates when you use your phone's built-in auto-rotate feature. If it rotated, then the app may be incompatible with Sideways Mode, so add the app to Sideways Mode's incompatible apps list.

If it didn't rotate, then the app probably doesn't support rotation. Sideways Mode is not intended for overriding other apps' screen orientation preferences, so you may need to look elsewhere.

### The screen won't rotate and I'm not getting any errors

Try the following:
1. Make sure you're using an app that supports rotation
1. If the phone's built-in rotation also doesn't work, then there's a problem with your phone
1. Restart the phone

### I uninstalled the app, and now the screen won't rotate anymore

Sideways Mode need to be turned off before you uninstall it.

Try one of the following to fix it:
* Install Sideways Mode, turn it on, and then uninstall it from within the app, or;
* Turn your phone's built-in auto-rotation on and off a few times, or;
* Restart your phone
