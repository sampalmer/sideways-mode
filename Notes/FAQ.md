# Frequently Asked Questions

### Why use this when I can just disable auto-rotate?

Sideways Mode lets you keep using auto-rotate, and adjusts it so the screen is rotated the right way when you're lying on your side. Auto-rotate is a useful feature, and disabling it is a workaround.

If that doesn't make sense, then watch the video on the store listing for a demonstration.

If you neither use auto-rotate nor use your phone while lying on your side, then you don't need Sideways Mode.

### Can I use this app to make auto-rotate use reverse portrait (AKA upside-down)?

No. That's not what Sideways Mode is for. Sideways Mode used to have an option to do this, but that was for compatibility reasons, and it has been removed for simplicity.

### Can you change the app to let me lock the screen in any orientation?

No. That's not what Sideways Mode is for. Sideways Mode used to have a way to do this, but it has been removed since it is unrelated to purpose, among other reasons.

### Why did you remove features in version 2.0.0?

For a few reasons:
* To focus the app on its core purpose. A lot of people were using the app for other reasons and asking me for support with them.
* Sideways Mode used to run 24/7, but Android 8 made it impossible to do this without showing a notification. Having a 24/7 notification is a bad experience, so Sideways Mode needed to be reworked.
* The code and user interface were getting complicated, so I removed unimportant features to simplify things.

### Why won't "incompatible apps" work when there is a dialog over the app?

This is a limitation of Android. See https://github.com/sampalmer/sideways-mode/issues/473 for more details.
